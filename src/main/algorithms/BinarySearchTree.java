package algorithms;


public class BinarySearchTree {

    class Node {
        int key;
        String value;
        Node left, right;

        public Node(int key, String value) {
            this.key = key;
            this.value = value;
            // leave left/right alone for now - some parents may not have children
        }

        public Node min() {
            if (left == null) {
                return this;
            } else {
                return left.min();
            }
        }
    }

    Node root; // declare a tree root

    public void insert(int key, String value) {
        // insertItem returns a node so set root as the return of insertItem
        root = insertItem(root, key, value);
    }

    public Node insertItem(Node node, int key, String value) {
        // create new node
        Node newNode = new Node(key, value);

        // if root is null - set root to newNode
        if (node == null) {
            node = newNode;
            return node;
        }

        if (key < node.key) {
            node.left = insertItem(node.left, key, value);
        }
        else {
            node.right = insertItem(node.right, key, value);
        }

        // return fully constructed node
        return node;
    }

    public String find(int key) {

        // find node starting at root
        Node node = find(root, key);

        return node == null ? null : node.value;
    }

    private Node find(Node node, int key) {

        // if node is null or key is a match return node
        if (node == null || node.key == key) {
            return node;
        } else if (key < node.key) {
            // search the left side of tree
            return find(node.left, key);

        } else if (key > node.key) {
            // search the right side of tree
            return find(node.right, key);
        }

        // no match by this point so return null
        return null;

    }

    public int findMinKey() {
        return findMinNode(root).key;
    }

    public Node findMinNode(Node node) {
        return node.min();
    }

    public void delete(int key) {
        // start at root
        root = delete(root, key);
    }

    public Node delete(Node node, int key) {
        // if node is null return null
        if (node == null) {
            return null;
        }

        // recursion to traverse down tree
        // if key < node.key -> set node.left = delete(node.left, key)
        if (key < node.key) {
            node.left = delete(node.left, key);
        } else if (key > node.key) {
            // else if key > node.key -> set node.right = delete(node.right, key)
            node.right = delete(node.right, key);
        } else {
            // we have a matched key @ node
            System.out.println("matched node: " + node.key);

            // Case 1: No children - set node to null
            if (node.left == null && node.right == null) {
                node = null;
            }

            // Case 2: 1 child - replace node to delete with it's only child
            else if (node.left == null) {
                node = node.right;
            } else if (node.right == null) {
                node = node.left;
            }

            // Case 3: 2 children - find min right and swap with node and then call self recursively
            else {
                Node minRight = findMinNode(node.right);
                System.out.println("minRight: " + minRight.key);

                // swap key/value from minRight into node marked for deletion
                node.key = minRight.key;
                node.value = minRight.value;
                prettyPrint();


                // now delete the node.right
                node.right = delete(node.right, key);
            }

        }


        // finally return node
        return node;

    }

    public void prettyPrint() {
        // Hard coded print out of binary tree depth = 3

        int rootLeftKey = root.left == null ? 0 : root.left.key;
        int rootRightKey = root.right == null ? 0 : root.right.key;

        int rootLeftLeftKey = 0;
        int rootLeftRightKey = 0;

        if (root.left != null) {
            rootLeftLeftKey = root.left.left == null ? 0 : root.left.left.key;
            rootLeftRightKey = root.left.right == null ? 0 : root.left.right.key;
        }

        int rootRightLeftKey = 0;
        int rootRightRightKey = 0;

        if (root.right != null) {
            rootRightLeftKey = root.right.left == null ? 0 : root.right.left.key;
            rootRightRightKey = root.right.right == null ? 0 : root.right.right.key;
        }

        System.out.println("     " + root.key);
        System.out.println("   /  \\");
        System.out.println("  " + rootLeftKey + "    " + rootRightKey);
        System.out.println(" / \\  / \\");
        System.out.println(rootLeftLeftKey + "  " + rootLeftRightKey + " " + rootRightLeftKey + "   " + rootRightRightKey);
    }


    /**
     * public print methods do not need to know about the root. Use private methods called from public methods
     * to hold logic and root awareness.
     */
    public void printInOrderTraversal() {
        inOrderTraversal(root);
    }
    private void inOrderTraversal(Node node) {
        if (node != null) {
            inOrderTraversal(node.left);
            System.out.println(node.key);
            inOrderTraversal(node.right);
        }

    }

    public void printPreOrderTraversal() {
        preOrderTraversal(root);
    }

    private void preOrderTraversal(Node node) {
        if (node != null) {
            System.out.println(node.key);
            preOrderTraversal(node.left);
            preOrderTraversal(node.right);
        }
    }

    public void printPostOrderTraversal() {
       postOrderTraversal(root);
    }

    private void postOrderTraversal(Node node) {
        if (node != null) {
            postOrderTraversal(node.left);
            postOrderTraversal(node.right);
            System.out.println(node.key);
        }

    }

}
