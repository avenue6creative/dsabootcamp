package algorithms;

public class BubbleSortRecursive {
    static void sort(int arr[], int n) {
        boolean swapped = false;

        // Base case
        if (n == 1) {
            return;
        }

        for (int i = 0; i < n - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
                swapped = true;
            }
        }

        if (swapped == false) {
            System.out.println("no need to swap");
            sort(arr, 1);
        } else {
            sort(arr, n - 1);
        }
    }

    public static void main(String[] args) {
//        int arr[] = {64, 34, 25, 12, 22, 11, 90};
        int arr[] = {4, 14, 25, 42, 82, 111, 190};
        sort(arr, arr.length);
        for (int i = 0; i < arr.length; i++) {
            System.out.printf("%d[%d]\n", i, arr[i]);
        }
    }



}
