package algorithms;

public class Fibonacci {
    private int[] memo = new int[1001];

    /**
     *
     * @param n the nth number in Fibonacci sequence
     * @return int Fibonacci value at n
     */
    static int naive(int n) {
        System.out.println("fib n: " + n);
        if (n <= 1) {
            return n;
        } else {
            return naive(n-1) + naive(n - 2);
        }
    }


    public int memoized(int n) {
        System.out.println("fib n: " + n);
        if (n <= 1) {
            return n;
        } else if (memo[n] == 0) {
            memo[n] = memoized(n - 1) + memoized(n - 2);
        }
        return memo[n];
    }
}

/**
 *
 * fib(3)
 * fib(2) + fib(1)
 * fib(1) + fib(0) + fib(1)
 * 1 + 0 + 1
 *
 *
 * fib(5)
 * fib(5) + fib(4)
 * fib(4) + fib(3)  + fib(3) + fib(2)
 * fib(3) + fib(2)  + fib(2) + fib(1)   + fib(2) + fib(1)   + fib(1) + fib(0)
 * fib(2) + fib(1)  + fib(1) + fib(0) + fib(1) + fib(0) + fib(0) + fib(0) + fib (1)
 * fib(1) + fib(0)  + fib(1)  + fib(1) + fib(0) + fib(1) + fib(0) + fib(0) + fib(0) + fib (1)
 * 1 + 1 + 1 + 1 + 1 = 5
 */
