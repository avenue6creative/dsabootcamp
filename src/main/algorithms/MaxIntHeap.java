package algorithms;

import java.util.Arrays;

public class MaxIntHeap {
    private int capacity = 10;
    private int size = 0; // num elements

    public int[] items = new int[capacity];


    /**
     * for the ith element:
     * parent = (i - 1) / 2
     * left child = 2i + 1
     * right child = 2i + 2
     */

    // indices
    private int parentIndex (int childIndex) { return (childIndex -1) / 2; }
    private int leftChildIndex (int parentIndex) { return 2 * parentIndex + 1; }
    private int rightChildIndex (int parentIndex) {return 2 * parentIndex + 2; }

    // getters
    private int leftChild(int index) { return items[leftChildIndex(index)]; }
    private int rightChild(int index) { return items[rightChildIndex(index)]; }
    private int parent(int index) { return items[parentIndex(index)]; }



    // hasChild checks
    // root has no parent so if parent index = 0 we know there is no parent
    private boolean hasParent (int index) { return parentIndex(index) >= 0; }

    // check that index is within num of elements contained in items[]
    private boolean hasLeftChild (int index) { return leftChildIndex(index) < size; }
    private boolean hasRightChild (int index) { return rightChildIndex(index) < size; }


    private void ensureCapacity() {
        // @TODO implement without Arrays util - copy array iter. into tmp array and replace og
        if (size == capacity) {
            // if maxxed out, double size of array with existing elements in place
            items = Arrays.copyOf(items, capacity * 2);
            capacity *= 2;
        }
    }

    // no args needed bc max is always at top (root)
    public int extractMax() {
        if (size == 0) {
            throw new IllegalStateException();
        }

        // take root
        int item = items[0];

        // swap root with smallest element by value, not index
        items[0] = items[size-1];

        // min is now at top and max at bottom
        size--;

        heapifyDown();
        return item;





    }

    private void heapifyDown() {
        // start at top - set index to root
        int index = 0;

        // while left child - no right without left
        while (hasLeftChild(index)) {

            // as long as there is a left - take larger of left or right
            int biggerChildIndex =
                    hasRightChild(index) && ( rightChild(index) > leftChild(index) ) ?
                    rightChildIndex(index) :
                    leftChildIndex(index);

            // if target greater than child we are sorted
            if (items[index] > items[biggerChildIndex]) {
                break;
            } else {
                // if target is smaller than child -> swap(heapItem, child)
                swap(index, biggerChildIndex);
            }


            // continue swapping down the smaller side with nex index
            index = biggerChildIndex;
        }




    }

    // check bottom element (last in array) and swap with parent if larger
    private void heapifyUp() {
        int index = size - 1;

        // while parents < index
        while (hasParent(index) && parent(index) < items[index]) {

            // remember swap takes indexes and not values
            swap(parentIndex(index), index);

            // update index
            index = parentIndex(index);
        }

    }


    public void insert(int item) {
        // ensure space for new item
        ensureCapacity();

        // insert always at end of items[]
        items[size] = item;

        // keep track of size counter
        size++;

        heapifyUp();

    }

    private void swap(int indexOne, int indexTwo) {
        int temp = items[indexOne];
        items[indexOne] = items[indexTwo];
        items[indexTwo] = temp;
    }


    // iteratively print items[]
    public void print() {
        for(int i = 0; i < capacity; i++) {
            System.out.println(i + "[" + items[i] + "]");
        }
    }

    public void size() {
        System.out.printf("size: %d", size);

    }



}
