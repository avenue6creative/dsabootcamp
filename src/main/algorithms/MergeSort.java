package algorithms;

// sorting @ o(n log n)
public class MergeSort {
    void merge(int arr[], int l, int m, int r) {
        // find sizes of two subarrays to be merged
        int size_l = m - l + 1;
        int size_r = r - m;

        // create temp []
        int L[] = new int[size_l];
        int R[] = new int[size_r];

        // copy data to temp arrays
        for (int i=0; i < size_l; ++i)
            L[i] = arr[l + i];
        for (int j=0; j < size_r; ++j)
            R[j] = arr[1 + m + j];

        // merge temp []

        // initial indexes of 1st and 2nd subarrays
        int i = 0, j = 0;

        // initial index of merged subarray
        int k = l;
        while(i < size_l && j < size_r) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        // copy remaining element of L[] if any
        while (i < size_l) {
            arr[k] = L[i];
            i++;
            k++;
        }

        // copy remaining element of R[] if any
        while (j < size_r) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    void sort(int arr[], int l, int r) {
        if (l < r) {
            // find mid point
            int m = (l+r)/2;

            // sort 1st and 2nd halves
            sort(arr, l, m);
            sort(arr, m+1, r);

            merge(arr, l, m, r);
        }
    }

    static void printArray(int arr[]) {
        int n = arr.length;

        for (int i=0; i < n; ++i)
            System.out.println(arr[i] + " ");
    }
}

