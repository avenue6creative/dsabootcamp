package algorithms;

public class MyMergeSort {

    /* find middle point and cut in half recursively until array size
       subarrays of 1.
       once size == 1, start merging back together until complete array merged
     */

    void merge(int arr[], int l, int m, int r) {
//        System.out.printf("l:%d, m:%d, r:%d", l, m, r);

        // find size of 2 subarrays for merge
        int size_l = m - l + 1;
        int size_r = r - m;

        // create temp[]
        int L[] = new int[size_l];
        int R[] = new int[size_r];

        // copy data into temp []
        for (int i=0; i<size_l; i++)
            L[i] = arr[l+i];
        for (int j=0; j<size_r; j++)
            R[j] = arr[m+j+1]; // start rt pointer 1 past mid point

        // merge temp[]

        // set initial indices of 2 arrays
        int i = 0, j = 0;

        int k = l; // k starts at left pointer
        while(i < size_l && j < size_r) {
            if(L[i] <= R[j]) {
                arr[k] = L[i]; //l <= r pointer so set l into array and move left pointer over 1
                i++;
            } else {
                arr[k] = R[j]; // set r into array and move
                j++;
            }
            k++; // move merged array to next index
        }

        while (i < size_l) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < size_r) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    void sort(int arr[], int l, int r) {
        if (l < r) { // will stop once l/r pointers are the same

            int m = (l+r)/2;

            // break down to smallest unit
            sort(arr, l, m); // left
            sort(arr, m+1, r); // right
            merge(arr, l, m, r);
        }

    }

    void printArray(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + "[" + arr[i] + "]");
        }
    }

}
