package algorithms;

public class MyQuickSort {
    // merge()

    // sort()

    public static void split(int[] arr, int lo, int hi) {
        int left = lo + 1; // left pointer is always +1 from pivot
        int right = hi;    // right pointer is right-most el

        int pivot = arr[lo];    // pivot is lo element


        // left idx and right idx pointers cannot cross
        while (left <= right) {

            while (arr[left] < pivot && left < right) {
                // move left pointer up until greater than pivot
                left++;
            }
            while (arr[right] > pivot) {
                // move right pointer down until less than pivot
                right--;
            }

            // swap l/r if l/r pointers have not crossed
            if (left <= right) {
                swap(arr, left, right);
                left++;
                right--;
            }
        }

        // @TODO: swap pointer with left - 1
        print(arr);

    }

    public static void swap(int[] arr, int l, int r) {
        int temp = arr[l];
        arr[l] = arr[r];
        arr[r] = temp;
    }

    public static void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(i + "[" + arr[i] + "]");
        }
    }


}
