package algorithms;

/**
 * divide and conquer
 */
public class QuickSort {

    private static <T extends Comparable<T>>
    int split(T[] list, int lo, int hi) {
        int left = lo + 1;
        int right = hi;

        T pivot = list[lo];

        return -1;
    }

    private static <T extends Comparable<T>> void sort(T[] list, int lo, int hi) {

        if ((hi-lo) <= 0) { // fewer than 2 items
            return;
        }

        int splitPoint = split(list, lo, hi);

        // recursive calls on l/r subarrays
        sort(list, lo, splitPoint -1);  // left subarray
        sort(list, splitPoint+1, hi);   // right subarray

    }


    private static <T extends Comparable<T>> void sort(T[] list) {
        if (list.length <= 1) {
            return;
        }
        sort(list, 0, list.length -1);
    }

}
