package datastructures;

import java.util.EmptyStackException;

public class DoubleLinkedList {
    Node head;

    static class Node {
        int data;
        Node prev, next;

        public Node(int d) {
            data = d;
        }
    }

    /**
     * Add new node front of list
     * @param data
     */
    public void push(int data) {
        // allocate Node
        Node newNode = new Node(data);

        // set next to head and prev to null (front of list)
        newNode.next = head;
        newNode.prev = null;

        // if head existed, point it's previous to newNode
        if (head != null) {
            head.prev = newNode;
        }

        // update head to be newNode
        head = newNode;
    }

    /**
     * Find node by data
     * @param data
     * @return
     */
    private Node findNode(int data) {
        Node matchedNode = null;

        if (head == null) {
            return matchedNode;
        }

        Node last = head;

        while (last != null) {
            if (last.data == data) {
                matchedNode = last;
                return matchedNode;
            }
            last = last.next;
        }

        // no match by this point
        return matchedNode;
    }

    /**
     * Given a node as next node, insert new node before the given node
     * @param node
     * @param data
     */
    public void insertBefore(int searchData, int data) {

        if (head == null) {
            throw new EmptyStackException();
        }
        // look for matching node.next and then update pointers
        Node matchedNode = findNode(searchData);

        if (matchedNode == null) {
            return;
        }

        Node newNode = new Node(data);
        Node prev = matchedNode.prev;

        // found match at head
        if (matchedNode == head) {
            System.out.print("found match at head\n");
            head.prev = newNode;
            newNode.next = matchedNode;
            head = newNode;
        } else {
            prev.next = newNode;
            newNode.prev = prev;
            newNode.next = matchedNode;
            matchedNode.prev = newNode;
        }
    }


    /**
     * Add node to end of linked list
     * @param data
     */
    public void append(int data) {
        Node newNode = new Node(data);

        // newNode is appended so make its next pointer null
        newNode.next = null;

        Node last = head;   // last will start at head

        if (head == null) {
            newNode.prev = null;
            head = newNode;
            return;
        }

        // traverse to end of list
        while (last.next != null) {
            // move to next node
            last = last.next;
        }

        // now at end of list
        last.next = newNode;
        newNode.prev = last;
    }

    public void print() {
        if (head == null) {
            return;
        }

        Node last = head;

        while (last != null) {
            System.out.println(last.data);
            last = last.next;
        }

    }


}
