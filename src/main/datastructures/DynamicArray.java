package datastructures;

public class DynamicArray<String> {
    private Object[] data;
    private int arrLength;
    private int initialCapacity;


    DynamicArray(int initialCapacity) {
        this.initialCapacity = initialCapacity;
        data = new Object[initialCapacity];
    }

    public String get(int index) {
        return (String)data[index];
    }

    public void set(int index, String value) { data[index] = value;}

    public void insert(int index, String value) {
        arrLength = getLength();

        // check if resize needed
        if (arrLength == initialCapacity) {
            resize();
        }

        // copy up from last element to insertion point
        for (int i = arrLength; i > index; i--) {
            // shjift elements to right
            data[i] = data[i - 1];
        }


        // insert new value
        set(index, value);
    }

    public boolean isEmpty() {
        return getLength() == 0;
    }

    public boolean contains(String value) {
        arrLength = getLength();

        for (int i = 0; i < arrLength; i++) {
            if (data[i] == value) {
                return true;
            }
        }
        return false;
    }


    /**
     * Resize array * 2. Requires copying to temp array and doubling original array
     */
    public void resize() {
        int newCapacity = initialCapacity * 2;
        Object[] newData = new Object[newCapacity];

        // build clone of original data with 2x size above
        for (int i = 0; i < arrLength; i++) {
            newData[i] = data[i];
        }

        // set original data to clone
        data = newData;

        initialCapacity = newCapacity;

    }

    /**
     *
     * @TODO null check will not work for size check. null should still count as value. A better isEmpty check is needed.
     *
     * @return void
     */
    public int getLength() {
        arrLength = 0;
        for (int i = 0; i < initialCapacity; i++) {
            if (data[i] != null) {
                arrLength++;
            }
        }
        return arrLength;
    }


    public void add(String value) {
        arrLength = getLength();

        // check if full capacity and resize if necessary
        if (arrLength == initialCapacity) {
            System.out.print("resize");
            resize();
        }

        // set new value at end of array
        set(arrLength, value);

    }

    public void delete(int index) {
        arrLength = getLength();

        // check if index exists
        if (data[index] != null && arrLength > 0) {
            //System.out.print("deleting: " + arrLength);

            // remove value and move elements between last element and index down 1
            for (int i = index; i < arrLength - 1; i++) {
                // move elements left
                data[i] = data[i + 1];
            }


            // check if last element in array and nullify
            data[arrLength - 1] = null;

            print();

        }

    }


    public void print() {
        for (int i=0; i < arrLength; i++) {
            System.out.println("data[" + i + "] = " + data[i]);
        }
    }

}
