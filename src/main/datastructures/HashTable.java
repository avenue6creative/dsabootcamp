package datastructures;

public class HashTable {

    private int INITIAL_SIZE = 16;

    // create HashEntry[]
    private HashEntry[] data; // data -> array of linked list HashEntry's

    // create HashEntry inner class and constructor (key, value)
    private class HashEntry {
        String key;
        String value;
        HashEntry next;

        HashEntry(String key, String value) {
            this.key = key;
            this.value = value;
            this.next = null;
        }
    }

    HashTable() {
        // on constructor build out array of HashEntry's
        data = new HashEntry[INITIAL_SIZE];
    }

    public void put(String key, String value) {
        /**
         * get index
         * create linked list entry with key value
         * if no entry at index add it
         * otherwise add to end of linked list
         */

        int index = getIndex(key);

        HashEntry entry = new HashEntry(key, value);


        if (data[index] == null) {
            data[index] = entry;
            return;
        }

        HashEntry entries = data[index];

        while (entries != null) {
            entries = entries.next;
        }

        entries.next = entry;

    }


    public String get(String key) {
        // get index by key
        int index = getIndex(key);

        // get current list of entries at key
        HashEntry entries = data[index];

        // if entries walk linked list at entry until match is found
        if (entries != null) {
            // while no key match and no next null
            while (!entries.key.equals(key) && entries.next != null) {
                // move to next entry in linked list
                entries = entries.next;
            }
            return entries.value;
        }

        // return null if no match

        return null;
    }


    public int getIndex(String key) {
        // get the hash code
        int hashCode = key.hashCode();
//        System.out.println("hashCode = " + hashCode);

        // % INITIAL_SIZE guarantees index will fit within initial size
        int index = (hashCode & 0x7fffffff) % INITIAL_SIZE; // force absolute value of negative indices
//        int index = hashCode % INITIAL_SIZE; // has negative values so do above
        System.out.println("index = " + index);

        // force collision for testing
        if (key.equals("John smith") || key.equals("Sandra Dee")) {
            index = 4;
        }

        return index;
    }

}
