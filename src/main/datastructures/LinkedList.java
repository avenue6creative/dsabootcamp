package datastructures;

public class LinkedList {

    // Java does not allow top-level static classes, so create inner Node class
    public class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    private Node head;

    /**
     * add node to front of linked list (head)
     *
     * @param data
     */
    public void addFront(int data) {
        Node newNode = new Node(data);

        // if no head, head -> new node
        if (head == null) {
            head = newNode;
            return;
        }

        // set new node pointer to current head
        newNode.next = head;

        // set head -> new node
        head = newNode;
    }

    /**
     * Add node to end of linked list (tail)
     *
     * @param data
     */
    public void addBack(int data) {
        // create new Node from data
        Node newNode = new Node(data);

        // if null head... set and return
        if (head == null) {
            head = newNode;
            return;
        }

        Node current = head;

        while (current.next != null) {
            // move to next node
            current = current.next;
        }

        // current is now tail so set new Node into next
        current.next = newNode;
    }

    public int getFirst() {
        return head.data;
    }

    public void deleteValue(int data) {
        // check head exists
        if (head == null) {
            throw new IllegalStateException("Cannot delete from an empty LinkedList");
        }

        // if match in head, move head to next node
        if (head.data == data) {
            head = head.next;
        }

        // if match in middle node
        Node current = head;

        while (current.next != null) {
            if (current.next.data == data) {
                // skip matched node by setting current next pointer +1 node from matched node
                current.next = current.next.next;
                return;
            }

            // no match so move current to next node
            current = current.next;
        }
    }


    public int getLast() {

        // check if head exists
        if (head == null) {
            throw new IllegalStateException("Cannot get last element of an empty list");
        }

        Node current = head;

        // walk linked list until next == null indicating tail O(n)
        while (current.next != null) {
            current = current.next;
        }

        return current.data;
    }


    public int size() {

        if (head == null) {
            return 0;
        }

        int count = 1;

        Node current = head;

        while (current.next != null) {
            current = current.next;
            count++;
        }

        return count;
    }

    public void clear() {
        head = null;
    }

    public void print() {
        Node current = head;
        while (current.next != null) {
            System.out.format("\n %d", current.data);
            current = current.next;
        }
        System.out.format("\n tail: %d\n", current.data);

    }

}

