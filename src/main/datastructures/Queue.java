package datastructures;

import java.util.EmptyStackException;

// FIFO
public class Queue {

    private class Node {
        private int data;
        private Node next;

        private Node(int data) {
            this.data = data;
        }

    }

    // make a head and tail
    private Node head;
    private Node tail;

    public boolean isEmpty() {
        return head == null;
    }

    public int peek() {
        return head.data;
    }


    public void add(int data) {
        Node newNode = new Node(data);

        // add to tail if exists
        if (tail != null) {
            tail.next = newNode;
        }


        // when tail null set tail to new node
        tail = newNode;

        // set head to tail when head null
        if (head == null) {
            head = tail;
        }

    }

    // Queue is FIFO so remove from head
    public int remove() {
        int data = head.data;

        if (head == null) {
            throw new EmptyStackException();
        }

        head = head.next;
        return data;
    }

    public void print() {
        if (tail == null) {
            return;
        }
        Node current = head;
        while (current != null) {
            System.out.println(current.data);
            current = current.next;
        }
    }

}


