package datastructures;

import java.util.EmptyStackException;

public class Stack {

    private class Node {
        private int data;
        private Node next;
        private Node (int data) {
            this.data = data;
        }
    }

    private Node head; // add/remove from here

    public void push(int data) {
        Node newNode = new Node(data);

        if (head == null) {
            head = newNode;
        }

        newNode.next = head;
        head = newNode;
    }

    // peak() - return data at head
    public int peek() {
        if (head == null) {
            throw new EmptyStackException();
        }
        return head.data;
    }

    // pop () - return head data, remove head, head moved to next item in stack
    public int pop() {
        if (head == null) {
            throw new EmptyStackException();
        }

        int popData = head.data;
        head = head.next;
        return popData;
    }

    public int size() {
        if (head == null) {
            throw new EmptyStackException();
        }
        int size = 0;

        head = head.next;

        Node current = head;

        while (head != null) {
            current = current.next;
            size++;
        }

        return size;
    }


}
