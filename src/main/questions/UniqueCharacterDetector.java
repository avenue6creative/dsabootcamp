package questions;

import java.util.Arrays;

public class UniqueCharacterDetector {
    static boolean isUnique(String str) {
        char letters[] = str.toCharArray();
        Arrays.sort(letters);

        // traverse making sure no neighbors are the same
        for (int i = 0; i < letters.length - 1; i++) {
            if (letters[i] == letters[i+1]) {
                return false;
            }
        }
        return true;
    }

    static boolean fastUnique(String str) {
        // if str > 128 there is a duplicate
        if (str.length() > 128) return false;

        boolean[] char_set = new boolean[128];

        for (int i = 0; i < str.length(); i++) {
            int val = str.charAt(i);
            if (char_set[val]) {
                return false;
            }
            char_set[val] = true;
        }
        return true;
    }
}
