package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTreeTest {
    private BinarySearchTree bst;

    @BeforeEach
    void Setup() {
        bst = new BinarySearchTree();
    }

    @Test
    void Insert() {
        bst.insert(3, "Neko");
        bst.insert(1, "Raphy");
        bst.insert(4, "Banjo");
        bst.insert(9, "Diego");


        Assertions.assertEquals("Neko", bst.find(3));
        Assertions.assertEquals("Raphy", bst.find(1));
        Assertions.assertEquals("Banjo", bst.find(4));
        Assertions.assertEquals("Diego", bst.find(9));
    }

    @Test
    void AVLTest() {
        bst.insert(7, "a");
        bst.insert(6, "a");
        bst.insert(5, "a");
        bst.insert(4, "a");
        bst.insert(3, "a");
        bst.insert(2, "a");
        bst.insert(1, "a");

        bst.prettyPrint();
    }

    @Test
    void MinCheck() {
        bst.insert(3, "Neko");
        bst.insert(1, "Raphy");
        bst.insert(4, "Banjo");
        bst.insert(9, "Diego");

        Assertions.assertEquals(1, bst.findMinKey());
    }

    @Test
    void Delete() {
        bst.insert(5, "e");
        bst.insert(3, "c");
        bst.insert(2, "b");
        bst.insert(4, "d");
        bst.insert(7, "g");
        bst.insert(6, "f");
        bst.insert(8, "h");

        bst.prettyPrint();
        bst.printInOrderTraversal();

        bst.delete(7);


    }
}