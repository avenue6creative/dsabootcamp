package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {
    private BubbleSort bubble;

    @Test
    public void BubbleSort() {
        int[] myArray = { 4, 8, 9, 0, 5, 1, 3 };

        Assertions.assertEquals(4, myArray[0]);

        bubble.sort(myArray);

        Assertions.assertEquals(0, myArray[0]);
        bubble.print(myArray);

    }

}