package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciTest {
    private Fibonacci fib;

    @BeforeEach
    public void Setup() {
    }

    @Test
    public void Find() {
//        Assertions.assertEquals(2, fib.naive(3));
//        Assertions.assertEquals(5, fib.naive(5));
        Assertions.assertEquals(8, fib.naive(6));
    }

    @Test
    public void MemoizedFib() {
        fib = new Fibonacci();
//        Assertions.assertEquals(2, fib.memoized(3));
//        Assertions.assertEquals(5, fib.memoized(5));
        Assertions.assertEquals(8, fib.memoized(6));
    }


}