package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxIntHeapTest {
    private MaxIntHeap maxHeap;

    @BeforeEach
    void Setup() {
        maxHeap = new MaxIntHeap();
    }

    @Test
    void InsertAndExtractMax() {

        maxHeap.insert(42);
        maxHeap.insert(29);
        maxHeap.insert(18);
        maxHeap.insert(35);


        Assertions.assertEquals(42, maxHeap.items[0]);
        Assertions.assertEquals(35, maxHeap.items[1]);

        Assertions.assertEquals(18, maxHeap.items[2]);
        Assertions.assertEquals(29, maxHeap.items[3]);

        Assertions.assertEquals(42, maxHeap.extractMax());
        Assertions.assertEquals(35, maxHeap.extractMax());
        Assertions.assertEquals(29, maxHeap.extractMax());
        Assertions.assertEquals(18, maxHeap.extractMax());


        maxHeap.print();
        maxHeap.size();
    }


}