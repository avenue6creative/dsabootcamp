package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortTest {

    private MyMergeSort sortOb;

    @BeforeEach
    void Setup() {
        sortOb = new MyMergeSort();
    }

    @Test
    void MergeSort() {
        int arr[] = {120, 11, 13, 5, 6, 7};

        sortOb.sort(arr, 0, arr.length-1);
        Assertions.assertEquals(5, arr[0]);
        Assertions.assertEquals(120, arr[5]);
        sortOb.printArray(arr);
    }

}