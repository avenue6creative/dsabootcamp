package datastructures;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoubleLinkedListTest {

    private DoubleLinkedList dll;

    @BeforeEach
    void Setup() {
        dll = new DoubleLinkedList();
    }

    @Test
    void Push() {
        dll.push(1);
        dll.push(2);
        dll.append(3);
        dll.print();

    }

    @Test
    void InsertBefore() {
        dll.push(3);
        dll.push(2);
        dll.insertBefore(2, 1);
        dll.print();
    }

}