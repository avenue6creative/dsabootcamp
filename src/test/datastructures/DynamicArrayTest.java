package datastructures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DynamicArrayTest {

    private DynamicArray testArray;

    @BeforeEach
    void setUp() {
        testArray = new DynamicArray(2);
    }

    @Test
    void GetAndSet() {
        testArray.set(0, "J");
        testArray.set(1, "A");
        testArray.print();
        Assertions.assertEquals("J", testArray.get(0) );
        Assertions.assertEquals("A", testArray.get(1) );
    }

    @Test
    void Add() {
        testArray.add("J");
        testArray.set(1, "A");

        testArray.add("K");
        testArray.add("E");
        testArray.print();
        Assertions.assertEquals("J", testArray.get(0) );
        Assertions.assertEquals("K", testArray.get(2) );
        Assertions.assertEquals("E", testArray.get(3) );
    }

    @Test
    void Insert() {
        testArray.add("J");
        testArray.set(1, "A");
        testArray.add("K");
        testArray.add("E");
        Assertions.assertEquals("K", testArray.get(2));

        testArray.insert(2, "D");
        Assertions.assertEquals("D", testArray.get(2));
    }

    @Test
    void Delete() {
        testArray.add("J");
        testArray.add("A");
        testArray.add("K");
        testArray.add("E");
        Assertions.assertEquals("A", testArray.get(1));

        testArray.delete(0);
        //Assertions.assertEquals(1, testArray.getLength());
    }

    @Test
    void EmptyCheck() {
        Assertions.assertTrue(testArray.isEmpty());
        testArray.add("J");
        Assertions.assertFalse(testArray.isEmpty());
    }

    @Test
    void Contains() {
        testArray.add("J");
        testArray.set(1, "A");
        testArray.add("K");
        testArray.add("E");
        Assertions.assertTrue(testArray.contains("K"));
        Assertions.assertFalse(testArray.contains("W"));

    }

}