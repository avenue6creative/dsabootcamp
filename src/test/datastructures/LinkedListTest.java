package datastructures;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    private LinkedList linkedList;

    @BeforeEach
    void setUp() {
        linkedList = new LinkedList();
    }


    @Test
    void AddFront() {
        linkedList.addFront(1);
        linkedList.addFront(2);
        linkedList.addFront(3);
        Assertions.assertEquals(3, linkedList.getFirst());
    }

    @Test
    void GetFirst() {
        linkedList.addFront(1);
        linkedList.addFront(2);
        Assertions.assertEquals(2, linkedList.getFirst());
    }

    @Test
    void GetLast() {
        Assertions.assertThrows(IllegalStateException.class, () -> linkedList.getLast());
        linkedList.addFront(1);
        linkedList.addFront(2);
        linkedList.addFront(3);
        Assertions.assertEquals(1, linkedList.getLast());
    }

    @Test
    void AddBack() {
        linkedList.addFront(1);
        linkedList.addFront(2);
        linkedList.addFront(3);
        Assertions.assertEquals(3, linkedList.getFirst());
        Assertions.assertEquals(1, linkedList.getLast());

        linkedList.addBack(9);
        Assertions.assertEquals(9, linkedList.getLast());

    }

    @Test
    void Size() {
        Assertions.assertEquals(0, linkedList.size());
        linkedList.addBack(1);
        Assertions.assertEquals(1, linkedList.size());
        linkedList.addFront(2);
        Assertions.assertEquals(2, linkedList.size());

    }

    @Test
    void Clear() {
        linkedList.addFront(1);
        linkedList.addFront(1);
        Assertions.assertEquals(2, linkedList.size());
        linkedList.clear();
        Assertions.assertEquals(0, linkedList.size());
    }

    @Test
    void deleteValue() {
        Assertions.assertThrows(IllegalStateException.class, () -> linkedList.deleteValue(1));
        linkedList.addBack(1);
        linkedList.addBack(2);
        linkedList.addBack(3);
        linkedList.addBack(4);
        linkedList.addBack(5);
        Assertions.assertEquals(5, linkedList.size());

        // delete at head
        linkedList.deleteValue(1);
        Assertions.assertEquals(4, linkedList.size());


        // delete middle
        linkedList.deleteValue(4);
        Assertions.assertEquals(5, linkedList.getLast());
        Assertions.assertEquals(3, linkedList.size());

        // delete at tail
        linkedList.deleteValue(5);
        Assertions.assertEquals(3, linkedList.getLast());
        Assertions.assertEquals(2, linkedList.size());

    }


}