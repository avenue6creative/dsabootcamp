package datastructures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QueueTest {
    private Queue queue;

    @BeforeEach
    void Setup() {
        queue = new Queue();
    }

    @Test
    void IsEmpty() {
        Assertions.assertTrue(queue.isEmpty());
    }

    @Test
    void Add() {
        queue.add(1);
        queue.add(2);
        Assertions.assertEquals(1, queue.peek());
    }

    @Test
    void Peek() {
        queue.add(1);
        queue.add(2);
        Assertions.assertEquals(1, queue.peek());
    }

    @Test
    void Remove() {
        queue.add(1);
        queue.add(2);
        queue.add(3);
        Assertions.assertEquals(1, queue.remove());
    }

}