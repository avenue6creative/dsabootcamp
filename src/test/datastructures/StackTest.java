package datastructures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {
    private Stack stack;

    @BeforeEach
    void Setup() {
        stack = new Stack();
    }

    @Test
    void PushAndPeek() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        Assertions.assertEquals(3, stack.peek());
    }

    @Test
    void EmptyPeek() {
        Assertions.assertThrows(EmptyStackException.class, () -> stack.peek());
    }

    void Pop() {
        Assertions.assertThrows(EmptyStackException.class, () -> stack.pop());
        stack.push(3);
        stack.push(2);
        stack.push(1);

        Assertions.assertEquals(3, stack.pop());
        Assertions.assertEquals(2, stack.peek());
        Assertions.assertEquals(2, stack.size());
    }


}