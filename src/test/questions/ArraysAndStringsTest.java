package questions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class ArraysAndStringsTest{
    private UniqueCharacterDetector charDetector;
    private PermutationDetector permDetector;


    @Test
    public void UniqueCharacterDetector() {
        Assertions.assertFalse(charDetector.isUnique("abzesbawet"));
        Assertions.assertTrue(charDetector.isUnique("abcde1"));
        Assertions.assertTrue(charDetector.fastUnique("abcezx"));
        Assertions.assertFalse(charDetector.fastUnique("abcezxa"));
    }

    @Test
    public void PermutationOfTheOther() {
        Assertions.assertTrue(permDetector.isPermutation("abc", "cba"));
//        Assertions.assertFalse(permDetector.isPermutation("abc", "c1abab"));
    }

}